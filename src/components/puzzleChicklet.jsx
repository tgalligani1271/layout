import React, { Component } from "react";

class PuzzleChicklet extends Component {
  handlePuzzleClick = () => {
    let nid = this.props.val.nid;
    this.props.handleOpen(nid);
  };

  render() {
    return (
      <div className="column">
        <div className="card" onClick={this.handlePuzzleClick}>
          <div className="card-image">
            <figure className="image is-4by3">
              <img
                src="http://localhost:3000/images/CrosswordUSAorig.svg"
                alt=""
              />
            </figure>
          </div>
          <span className="is-small">{this.props.title}</span>
        </div>
      </div>
    );
  }
}

export default PuzzleChicklet;
