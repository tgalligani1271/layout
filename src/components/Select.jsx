import React, { Component } from "react";

class Select extends Component {
  render() {
    return (
      <div className="columns">
        <div className="column is-2" />
        <div className="column is-8">
          <div className="field">
            <p className="control">
              <span className="select is-medium">
                <select>
                  {this.props.options.map((x, y) => (
                    <option key={y}>{x}</option>
                  ))}
                </select>
              </span>
            </p>
          </div>
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {}
}

export default Select;
