import React, { Component } from "react";
import { BrowserRouter as Router, Link, Route } from "react-router-dom";
import Home from "../../../pages/Home";
import Solve from "../../../pages/Solve";

class Nav extends Component {
  state = {
    navClass: "is-active"
  };

  /* This could be more elegent */
  deriveNavClass = (defaultClasses, statusClass) => {
    return defaultClasses + statusClass;
  };

  render() {
    return (
      <Router>
        <div>
          <nav className="navbar is-primary">
            <div className="container">
              <div className="icon-twitter" />
              <div className="icon-facebook" />
              <div className="navbar-brand">
                <a className="navbar-item" href="/">
                  PUXXLER
                </a>
                <span
                  className={this.deriveNavClass(
                    "navbar-burger burger ",
                    this.props.toggleClass
                  )}
                  onClick={() => this.props.onToggleBurger(this.props)}
                >
                  <span />
                  <span />
                  <span />
                </span>
              </div>
              <div
                id="navMenu"
                className={this.deriveNavClass(
                  "navbar-menu ",
                  this.props.toggleClass
                )}
              >
                <div className="navbar-end">
                  {this.props.links.map((x, y) => (
                    <Link key={y} to={x.path} className="navbar-item">
                      {x.text}
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          </nav>
          <Route path="/" component={Home} exact />
          <Route path="/solve" component={Solve} exact />
          <Route path="/solve/:id" component={Solve} />
        </div>
      </Router>
    );
  }
}

export default Nav;
