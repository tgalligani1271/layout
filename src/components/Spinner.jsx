import React, { Component } from "react";

class Spinner extends Component {
  state = {};
  render() {
    return (
      <section>
        <div className="container">
          <div class="notification is-primary">
            <a className="button is-primary is-loading is-large">Loading</a>
          </div>
        </div>
      </section>
    );
  }
}

export default Spinner;
