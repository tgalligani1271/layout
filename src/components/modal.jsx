import React, { Component } from "react";
import Spinner from "./Spinner";

class modal extends Component {
  state = {
    show: false
  };

  hideModal = () => {
    return this.props.show ? "modal is-active" : "modal";
  };

  manageContent = () => {
    return this.props.content === undefined ? <Spinner /> : this.props.content;
  };

  render() {
    return (
      <div className={this.hideModal()}>
        <div className="modal-background" />
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">{this.props.title}</p>
            <button
              className="delete"
              aria-label="close"
              onClick={this.props.handleClose}
            />
          </header>
          <section className="modal-card-body">
            <article className="card is-large is-warning">
              {this.manageContent()}
            </article>
          </section>
          <footer className="modal-card-foot">
            <button className="button is-success">OK</button>
          </footer>
        </div>
      </div>
    );
  }
}

export default modal;
