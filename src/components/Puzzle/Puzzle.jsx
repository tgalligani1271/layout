import React, { Component } from "react";
import Grid from "./Grid";
import Clues from "./Clues";

class Puzzle extends Component {
  state = {
    gridSquareIndex: null,
    prevGridSquareIndex: null,
    blueSquares: [],
    blueSquaresDir: "across",
    squareLetter: null,
    squareLetters: []
  };

  getClues = dir => {
    if (
      this.props.puzzle.body !== null &&
      this.props.puzzle.body !== undefined
    ) {
      let { across, down } = this.props.puzzle.body.clues;
      return dir === "across" ? across : down;
    } else {
      return [];
    }
  };

  puzzleGridClick = gridSquareIndex => {
    let dir = this.dirToggle(gridSquareIndex, this.state.blueSquaresDir);
    let blueSquares = this.setBlueSquares(gridSquareIndex, dir);
    this.setState({
      gridSquareIndex: gridSquareIndex,
      blueSquares: blueSquares,
      prevGridSquareIndex: gridSquareIndex,
      blueSquaresDir: dir,
      squareLetter: null
    });
  };

  dirToggle(gridSquareIndex, dir) {
    if (gridSquareIndex === this.state.prevGridSquareIndex) {
      if (dir === "across") {
        dir = "down";
      } else {
        dir = "across";
      }
    }
    return dir;
  }

  setBlueSquares = (index, dir) => {
    switch (dir) {
      case "down":
        return this.setBlueSquaresDown(index);
      case "across":
        return this.setBlueSquaresAcross(index);
      default:
        return "across";
    }
  };

  setBlueSquaresAcross = index => {
    let clueId = `A${this.props.puzzle.squares[index].acrossClue}`;
    let clueFound = this.props.puzzle.body.clues.across.find(e => {
      return e.id === clueId;
    });
    return clueFound.squares;
  };

  setBlueSquaresDown = index => {
    let clueId = `D${this.props.puzzle.squares[index].downClue}`;
    let clueFound = this.props.puzzle.body.clues.down.find(e => {
      return e.id === clueId;
    });
    return clueFound.squares;
  };

  handleKeyPress = event => {
    let letter = event.key;
    console.log("letter", letter);
    let handleKeyLetter = this.state.squareLetters;
    handleKeyLetter[this.state.gridSquareIndex] = letter;
    let nextSquare =
      this.state.blueSquaresDir === "down"
        ? this.state.gridSquareIndex + 15
        : this.state.gridSquareIndex + 1;

    this.setState({
      //note - this needs to be set to the next logical square
      // gridSquareId: id,
      squareLetters: handleKeyLetter,
      gridSquareIndex: nextSquare
    });
    // this.advanceGridSquare();
  };

  advanceGridSquare() {
    if (this.state.gridSquareIndex !== null) {
      let sqid,
        prevSqid = this.state.gridSquareIndex;
      let dir = this.state.blueSquaresDir;
      let rowcols = this.state.gridSquareIndex;

      if (dir === "down") {
        rowcols[0] = Number(rowcols[0]) + 1;
      } else {
        rowcols[1] = Number(rowcols[1]) + 1;
      }
      sqid = `r${rowcols[0]}c${rowcols[1]}`;
      let blueSquares = this.setBlueSquares(sqid, dir);
      this.setState({
        prevGridSquareId: prevSqid,
        gridSquareId: sqid,
        blueSquares: blueSquares,
        blueSquaresDir: dir
      });
    }
  }

  render() {
    return (
      <div className="container">
        <div className="columns">
          <div className="column is-1" />
          <div className="column is-10">
            <div className="notification is-light">{this.props.id}</div>
          </div>
          <div className="column is-1" />
        </div>
        <div className="columns" />
        <div className="columns">
          <div className="column is-6">
            <section
              className="container cw_container"
              id="cw_container"
              style={{ maxWidth: 490 }}
            >
              <Grid
                puzzle={this.props.puzzle}
                puzzleGridClick={this.puzzleGridClick}
                handleKeyPress={this.handleKeyPress}
                activeSquare={this.state.gridSquareIndex}
                blueSquares={this.state.blueSquares}
                squareLetters={this.state.squareLetters}
              />
            </section>
          </div>
          <div className="column is-4">
            <Clues direction="across" clues={this.getClues("across")} />
            <Clues direction="down" clues={this.getClues("down")} />
          </div>
        </div>
      </div>
    );
  }
}

export default Puzzle;
