import React, { Component } from "react";
class Clues extends Component {
  extractClues = () => {
    let clues = [];
    clues = this.props.clues.map((clue, i) => {
      return (
        <li className="clue has-text-weight-normal" id={clue.id} key={clue.id}>
          {clue.clue}
        </li>
      );
    });
    return clues;
  };

  render() {
    return (
      <section className="clue-container" id={this.props.direction}>
        {this.props.direction}
        <div className="notification is-black is-paddingless" />
        <ul className="is-small">{this.extractClues()}</ul>
      </section>
    );
  }
}

export default Clues;
