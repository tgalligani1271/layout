import React, { Component } from "react";
import GridSquare from "./GridSquare/index";
import _ from "lodash";

class Grid extends Component {
  state = {
    puzzleIsEmpty: true,
    width: 40,
    height: 40,
    puzzle: {},
    showNumbers: true,
    showLetters: false,
    gridClickToggleAcross: true,
    clickedGrid: null
  };

  buildSquares = () => {
    let gridClick = index => {
      this.props.puzzleGridClick(index);
    };

    let handleKeyPress = event => {
      this.props.handleKeyPress(event);
    };

    let gs = [];
    let { activeSquare, blueSquares, squareLetter } = this.props;

    if (
      this.props.puzzle.body !== undefined &&
      this.props.puzzle.body !== null
    ) {
      let { cols, rows } = this.props.puzzle.body.size;
      let { squares } = this.props.puzzle;
      let { squareLetters } = this.props;
      let { width, height, showNumbers, showLetters } = this.state;

      _.forEach(squares, function(square, index) {
        let classs = square.class;
        let x = index % cols;
        let posx = x * width;
        let y = Math.floor(index / cols);
        let posy = y * height;
        let id = "r" + y + "c" + x;
        classs = blueSquares.includes(index) ? classs + " bluebox" : classs;
        classs = activeSquare === index ? " active" : classs;
        let letter = squareLetters[index];

        let n = 1;
        gs[index] = (
          <GridSquare
            id={id}
            posx={posx}
            posy={posy}
            showNumber={showNumbers}
            showLetter={showLetters}
            letter={square.letter}
            number={n}
            classs={classs}
            cols={cols}
            rows={rows}
            index={index}
            key={index}
            gridnum={square.number}
            gridClick={gridClick}
            handleKeyPress={handleKeyPress}
            squareLetter={letter}
          />
        );
      });
    } else {
      gs = (
        <g key="99">
          <text x="150" y="90" id="clue1">
            No Puzzle at This Location
          </text>
        </g>
      );
    }
    return gs;
  };

  render() {
    return (
      <svg id="crossword" viewBox="0 0 605 605" ref={"grid"}>
        {this.buildSquares()}
      </svg>
    );
  }
}

export default Grid;
