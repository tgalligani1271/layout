import React, { Component } from "react";

class GridSquare extends Component {
  state = {
    width: 40,
    height: 40
  };

  handleKeyPress = event => {
    this.props.handleKeyPress(event);
  };

  handleGridClick = () => {
    let { index } = this.props;
    this.props.gridClick(index);
  };

  buildSquare = () => {
    const { height, width } = this.state;
    const { id, posx, posy, gridnum, squareLetter } = this.props;

    return (
      <g
        id={id}
        onClick={this.handleGridClick}
        onKeyPress={this.handleKeyPress}
        tabIndex="0"
      >
        <rect
          value={posx}
          className={this.props.classs}
          x={posx}
          y={posy}
          width={width}
          height={height}
        />
        <text x={posx + 2} y={posy + 13} id="clue1">
          {gridnum === 0 ? null : gridnum}
        </text>
        <text
          className="inputText"
          x={posx + 20}
          y={posy + 35}
          textAnchor="middle"
        >
          {squareLetter}
        </text>
      </g>
    );
  };

  render() {
    return this.buildSquare();
  }
}

export default GridSquare;
