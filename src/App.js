import React, { Component } from "react";
import "./App.css";
import Nav from "./components/Header/Nav/Nav.jsx";
import routes from "./config/routes.json";

class App extends Component {
  state = {
    links: routes.links,
    burgerState: ""
  };

  componentDidMount() {}

  handleToggleBurger = () => {
    let burgerClass = this.state.burgerState === "" ? "is-active" : "";
    this.setState({ burgerState: burgerClass });
  };

  render() {
    return (
      <div>
        <Nav
          key={0}
          links={this.state.links}
          toggleClass={this.state.burgerState}
          onToggleBurger={this.handleToggleBurger}
        />
      </div>
    );
  }
}

export default App;
