import request from "superagent";

// A function that returns a promise to resolve into the data //fetched from the API or an error
const getPuzzleById = nid => {
  return request.get(`http://localhost/content/puzzle/${nid}`);
};
export default getPuzzleById;
