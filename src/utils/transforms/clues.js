import _ from "lodash";
//Functions for the clues

const cluesFindAssocSquares = (clueId, puzzle) => {
  let clueSquares = [];
  let { squares } = puzzle;

  _.forEach(squares, (square, i) => {
    if (clueId === `A${square.acrossClue}`) {
      clueSquares.push(i);
    }
    if (clueId === `D${square.downClue}`) {
      clueSquares.push(i);
    }
  });

  return clueSquares;
};

const cluesRefactor = puzzle => {
  let { clues } = puzzle.body;
  let across = [];
  let down = [];

  _.forEach(clues.across, (clue, i) => {
    let clueId = `A${clue.split(".")[0]}`;
    let clueSquares = cluesFindAssocSquares(clueId, puzzle);
    across.push({ id: `${clueId}`, clue: clue, squares: clueSquares });
  });

  _.forEach(clues.down, (clue, i) => {
    let clueId = `D${clue.split(".")[0]}`;
    let clueSquares = cluesFindAssocSquares(clueId, puzzle);
    down.push({ id: `${clueId}`, clue: clue, squares: clueSquares });
  });

  puzzle.body.clues.across = across;
  puzzle.body.clues.down = down;

  return puzzle;
};

export { cluesRefactor };
