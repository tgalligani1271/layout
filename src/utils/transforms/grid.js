//Refactor puzzle grid for ease of manipulation

const initSquares = size => {
  let sq = [];
  for (let r = 0; r < size.rows; r++) {
    for (let c = 0; c < size.cols; c++) {
      sq.push({ row: r, col: c, id: `r${r}c${c}` });
    }
  }
  return sq;
};

const addLettersAndClues = (sq, size, grid, gridnums) => {
  //Now, add the letters, numbers and classes to the squares
  let acrossIndex = 0;
  for (let i = 0; i < sq.length; i++) {
    sq[i].letter = grid[i];
    sq[i].number = gridnums[i];
    sq[i].class = grid[i] === "." ? "black-square" : "white-square";
    if (grid[i - 1] === "." || sq[i].col === 0) acrossIndex = gridnums[i];
    sq[i].acrossClue = grid[i] === "." ? null : acrossIndex;
  }

  //To assoc the down clues we must iterate through columns
  let currentClue = 1;
  for (let c = 0; c < size.cols; c++) {
    for (let r = 0; r < size.rows; r++) {
      let i = r * 15 + c;

      if (r === 0) {
        currentClue = sq[i].number;
      }

      if (sq[i].letter === ".") {
        currentClue = null;
      } else {
        if (currentClue === null) {
          currentClue = sq[i].number;
        }
      }
      sq[i].downClue = currentClue;
    }
  }
  return sq;
};

const getRow = (n, size) => {
  return n % size.cols;
};

const getCol = (n, size) => {
  return Math.floor(n / size.cols);
};

const getUpSquare = (index, sqs, size) => {
  let i = index - size.cols;
  return index > -1 ? sqs[i] : null;
};

const getDownSquare = (index, sqs, size) => {
  let i = index - size.cols;
  return index > -1 ? sqs[i] : null;
};

const getPrevSquare = (index, sqs, size) => {
  return index++;
};

const getNextSquare = (index, sqs, size) => {
  return index--;
};

const gridRefactor = puzzle => {
  let { grid, gridnums, size } = puzzle.body;

  //First, establish the row, col, and id for each square
  let sq = initSquares(size);

  //Add letters and clues
  sq = addLettersAndClues(sq, size, grid, gridnums);

  puzzle.squares = sq;
  return puzzle;
};

export {
  gridRefactor,
  getRow,
  getCol,
  getUpSquare,
  getDownSquare,
  getNextSquare,
  getPrevSquare
};
