import request from "superagent";
import _ from "lodash";

const fetchPuzzles = request.get("http://localhost:80/content");

export default fetchPuzzles;
