import React, { Component } from "react";
import PuzzleChicklet from "../components/puzzleChicklet";
import _ from "lodash";
import request from "superagent";
import Modal from "../components/modal";

class Home extends Component {
  state = {
    puzzles: [],
    modal: false,
    puzzlecontent: null
  };

  fetchPuzzles = () => {
    let n = [];
    request
      .get("http://localhost:80/content/list")
      .then(res => {
        _.forEach(res.body, function(val, i) {
          n.push(val);
        });
        return n;
      })
      .then(n => {
        this.setState({ puzzles: n });
      });
  };

  openSolve = p => {
    let url = `/solve/${p}`;
    window.location.assign(url);
  };

  showModal = p => {
    this.setState({ modal: true });
    this.setState({ puzzlenumber: p });
    this.setState({ puzzlecontent: null });
    let url = "http://localhost:80/content/puzzle/" + { p };
    this.setState({ puzzlecontent: url });
  };

  hideModal = () => {
    this.setState({ modal: false });
    this.setState({ puzzlecontent: null });
  };

  componentDidMount() {
    this.fetchPuzzles();
  }

  render() {
    let deez_puzzles = this.state.puzzles.map((val, i) => {
      return (
        <PuzzleChicklet
          key={i}
          title={val.title}
          val={val}
          // handleOpen={this.showModal}
          handleOpen={this.openSolve}
          content={val.title}
        />
      );
    });

    return (
      <section className="content">
        <Modal show={this.state.modal} handleClose={this.hideModal} />
        <div className="container">
          <div className="columns">
            <div className="column is-2" />
            <div className="column is-8">
              <div className="container">
                <div className="notification is-white">
                  <h1 className="title is-3">PUXXLER</h1>
                </div>
              </div>
              <div className="notification is-light">
                Solve these puzzles from <strong>The NY Times</strong>.
              </div>
              <div className="columns">
                {deez_puzzles}
                {/* {
                  <PuzzleChicklet
                    puzzles={this.state.puzzles}
                    handleOpen={this.showModal}
                  />
                } */}
              </div>
              <div className="notification is-light" />
            </div>
            <div className="column is-2" />
          </div>
          <div className="columns">
            <div className="column is-one-quarter" />
            <div className="is-three-quarters" />
          </div>
        </div>
      </section>
    );
  }
}

export default Home;
