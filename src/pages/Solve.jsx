/*
*
*/

import React, { Component } from "react";
import Puzzle from "../components/Puzzle/Puzzle";
import { getPuzzleById } from "../utils/promises";
import { gridRefactor, cluesRefactor } from "../utils/transforms";

class Solve extends Component {
  state = {
    nid: null,
    puzzle: {}
  };

  componentDidMount() {
    if (this.props.match.params.id !== null) {
      getPuzzleById(this.props.match.params.id).then(res => {
        let puzzle = gridRefactor(res);
        this.setState({ puzzle: cluesRefactor(puzzle) });
      });
    } else {
      // load list of puzzles
    }
  }

  render() {
    return (
      <div className="column is-4">
        <Puzzle puzzle={this.state.puzzle} id={this.props.match.params.id} />
      </div>
    );
  }
}

export default Solve;
