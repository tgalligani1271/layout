/*
*
*/

import React, { Component } from "react";
import request from "superagent";
import Puzzle from "../components/Puzzle/Puzzle";

class Solve extends Component {
  state = {
    puzzle: {},
    options: [],
    nid: null,
    showNumbers: true
  };

  fetchPuzzleById = nid => {
    let url = `http://localhost/content/puzzle/${nid}`;
    request.get(url).then(res => {
      this.setState({ puzzle: res });
    });
  };

  componentDidMount() {
    if (this.props.match.params.id !== null) {
      let n = this.fetchPuzzleById(this.props.match.params.id);
    } else {
      // load list of puzzles
    }
  }

  render() {
    return (
      <div className="column is-4">
        <Puzzle puzzle={this.state.puzzle} id={this.props.match.params.id} />
      </div>
    );
  }
}

export default Solve;
